'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

//Selected Elements 
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');


/*
/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES


const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

/////////////////////////////////////////////////
const arr = [1,2,3,4,5,6,7,8];
console.log(arr.slice(2,5));
console.log(arr.slice(5));
console.log(arr);

// slice is used to create shallow copy of an array
const arrShallowCopy1 = arr.slice()
const arrShallowCopy2 = [...arr]

console.log(arr.splice(2,5,"kobe")); // o/p: [3,4,5,6,7]
console.log(arr); // o/p: [1,2,"Kobe",8]

console.log(arr.reverse());
console.log(arr)

for (let i=0;i<arr.length;i++) console.log(i,typeof i)
for (let i in arr) console.log(i,typeof i)
for (let i of arr) console.log(i,typeof i)

arr.forEach(function(curr){
  arr.push(curr*10);  
})
console.log(arr)
let sum = 0;
[1,2,3,4].forEach(function(ele,index,arr){
  sum += arr[index];
})

// forEach in Maps,sets
const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);
currencies.forEach(function(v,k,m){
  console.log(k,'----->',v,m);
})
const ste = new Set([10,20,30,40,20,30,40,50,20,40,20,40]);
console.log(ste)
ste.forEach(function(v,k,s){
  console.log(v,k,s)
})
*/

const array1 = [1,2,3,4,5,6,7,8,9,10];
const mul = array1.reduce(function(acc,curr){
  return acc*curr
},1);
console.log(mul);

//calculate maximum value from reduce 
const arr2 = [200, 450, -400, 3000, -650, -130, 70, 1300];
const maxValue = arr2.reduce(function(acc,curr){
  if (curr>acc) return curr
  else return acc
},arr2[0]);
console.log(maxValue);


// lect -155 find() method
const find1 = array1.find((ele) => ele>2);
const find2 = array1.find((ele) => ele>22);
console.log(find1);
console.log(find2);

console.log(accounts.find((ele) => ele.owner === 'Jessica Davis'));

for (let ele of accounts){
  if (ele.owner == "Jessica Davis") console.log(ele); 
}

const nestedArr = [0,1,[2,3,[4,5]],6] // nested upto deapth =2
console.log(nestedArr)
const nestedArr1 = nestedArr.flat();
console.log(nestedArr1);
const nestedArr2 = nestedArr.flat(2);
console.log(nestedArr2)
const nestedArr3 = nestedArr.flat(100);
console.log(nestedArr3);

console.log(accounts.map(ele => ele.movements).flat())

const q1 =[1,2,3];
const q2 = [7,8,9];
const qq = [q1,q2];

const unsorted = [1,200,40,5];
unsorted.sort( (a,b) => b-a)  // descending order
console.log(unsorted);   

// array() constructor method
const failedArray = new Array(4);
console.log(failedArray.length);
console.log(failedArray);
console.log(failedArray[0]);

failedArray.fill(420);
console.log(failedArray)

const failedArray1 = new Array(10).fill(-99,0,5).fill(0,5,10);
console.log(failedArray1)

// Array.from() method
const ran = Array.from("abc",(ele,ind) => ele+ind)
console.log(ran)

// range() method of python
console.log(Math.floor(Math.random()*6))

const diceArrayOf100Occurences = Array.from({length:100}, function(_){
  return Math.floor(Math.random()*7);
})
console.log(diceArrayOf100Occurences);
// how many times 6 occured
let sixOccur =0;
const z = diceArrayOf100Occurences.reduce(function(acc,ele){
  if (acc == 6) {
    // console.log(acc)
    return acc;
  }
  else return acc;
},0);
console.log(z)