'use strict';

// Generating a random integer between 0 and 20 (both inclusive)
let randomNum = Math.floor(Math.random()*20 + 1);

// Updating Score
let score = 20;


// function that will be called when the user clicks the "check" button
const clickEvent = function(){
    

    let guessedNumber = Number(document.querySelector('.guess').value);
    // When there is no input
    if (!guessedNumber  ) {
        document.querySelector(".message").textContent = "Please Enter a valid Number ✔";
    }
    // When the guessed number is higher  or lower
    else if (guessedNumber != randomNum){
        document.querySelector(".message").textContent = guessedNumber > randomNum ? "Your Guess is Too High 🚀": "Your Guess is Too Low 🌂"
        document.querySelector(".score").textContent = --score;
        }    

    // When the player wins
    else {
        document.querySelector(".message").textContent = "🤑🤑 Congratulations You've Won 🤑🤑";
        document.querySelector("body").style.backgroundColor = 'green';
        if (Number(document.querySelector(".highscore").textContent) < Number(document.querySelector(".score").textContent)) {
            document.querySelector(".highscore").textContent = document.querySelector(".score").textContent;
            document.querySelector(".number").textContent = randomNum;
        } 
    }
    // Adding "Game Over" Condition
    if (score==-1){
        document.querySelector(".message").textContent = "💀 Game Over 💀 \n You failed to guess the number";
        endGame();
    }
    
}
// function executed when conditions like-gameover, again and win happens
const endGame = function () {
        score =20;  // initialize score to "20" 
        randomNum = Math.floor(Math.random()*21);
        document.querySelector(".number").textContent = "?";
        document.querySelector(".score").textContent = 20;
        document.querySelector(".guess").value =""; // Empty the input box
}
   

//  function that will be called when the user clicks the "Again" button
const again = function(){
    endGame();
    document.querySelector("body").style.backgroundColor = '#222';
    document.querySelector(".message").textContent = "Start guessing...";
}

// Manipulating user input
document.querySelector('.btn.check').addEventListener('click',clickEvent);
document.querySelector('.btn.again').addEventListener('click',again);
