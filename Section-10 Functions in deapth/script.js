'use strict';
const print = function(qqq =1,a){
    console.log(qqq,a);
}
print(222,999);
print(222); // rule1: parameters are accepted positionally
print(undefined,444); // o/p: 1 444 ==> rule3: to skip parameters put undefined during call

// lec 130 - callback functions
const callback = function(str){
    const [first,...rest] = str.split(" ");
    return [first.toUpperCase(),...rest].join(" ");
}
const higherOrder1 = function(str,fn){
    console.log(`Original String :${str}`);
    console.log(`Transformed String :${fn(str)}`);
    console.log(`Transformed by :${fn.name}`);
}
higherOrder1("this is a cypher" , callback);
/* O/P:
Original String :this is a cypher            script.js:15:13
Transformed String :THIS is a cypher         script.js:16:13
Transformed by :callback                     script.js:17:13
 */

// iife
(function (){
    console.log("this function will never be called again");
})();

// closures
const secureBooking = function() {
    let passengerCount = 0;
    return function() {
        passengerCount++;
        console.log(passengerCount);
    };
};
const book = secureBooking();
book();
book();
book();

console.dir(book);