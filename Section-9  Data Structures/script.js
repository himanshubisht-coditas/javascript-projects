'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
};


/// Destructuring Arrays
const arr = [1,2,3];

// Manual Destructuring
const a1 = arr[0];
const a2 = arr[1];
const a3 = arr[2];

console.log(a1,a2,a3);  // expected o/p : 1 2 3

// Array-Destructuring
const [b1,b2,b3] = arr;
console.log(b1,b2,b3);
console.log(arr);  // original array is unaffected

//
const [first,second] = restaurant.categories;
console.log(first,second);

// leaving a hole
const [Italian, , ,organic] = restaurant.categories;
console.log(Italian,organic);

// Swapping Traditional way
let a=10,b=20;
const temp =a;
a=b;
b=temp;
console.log(a,b);

// Swapping Destrucuting way
let c=10,d=20; 
[d,c] = [c,d];
console.log("the value of c now is",c);
console.log("the value of d now is",d);

// a function returning array can be immediately destructured into variables
const arr_converter = function (){
  const returned_arr = [];
  for (let i of arguments){
    if (i) returned_arr.push(i); 
  }
  return returned_arr;
}
const [var1,var2,var3,var4,var5,var6] = arr_converter(10,20,30,40,50,60);
console.log(var1,var2,var3,var4,var5,var6);

// destructuring in nested arrays
const bars = [1,3,[2,5]];
const [bar, ,[nest1,nest2]] = bars;
console.log(bar,nest1,nest2);

//default values
const vv = [1,2,3,4]
const [v1 , v2 , v3 , v4 , v5 ] = vv;
console.log("line 87 ---------->" , v1,v2,v3,v4,v5);

// we got v5 as uundefined
// to avoid this we can set a default value
const [v11 , v22 , v33 , v44 , v55 = 99999 ] = vv;
console.log("line 92 ---------->" , v11,v22,v33,v44,v55);


// Destucuting objects
const person = {
  "firstName" : "Kobe",
  "age" : 25,
  "isMarried" : true,
  "profession" : "sports",
  "gameStreak" : {
    "game1" : "win",
    "game2" : "lost",
    "game3" : "win",
    "game4" : "win",
    "game5" : "lost",
    "game6" : "win"
  },
};

const {firstName,age} = person;
console.log(firstName);
console.log(age);
// console.log(profession)  ==> raises error coz we did'nt deconstruct it

// to give property names
const {isMarried : x1 , profession:x2} = person;
console.log(x1,x2);

// default values
const {career:x3} = person;
console.log(x3);  // we got undefined as the "career" key was not there

// to avoid this we can set a default value
const {career:x4 = "basketball"} = person;
console.log(x4);

// mutating objects
let radius = 10;
const obj = {'radius':20,"pi":3.14};
( {radius} = obj);
console.log(radius);

// nested object destructuring
const {gameStreak:{game1,game2:g2}} = person;
console.log(game1,g2); // g2 name changed from game2 to g2


// spread operator
const arr1 = [7,8,9];
const badArr1 = [1,2,arr1[0],arr1[1],arr1[2]];
const goodArr1 = [1,2,...arr1];
console.log(goodArr1);
console.log(badArr1);

console.log(...arr1);

// for concatenation arr = [...arr1 , ...arr2];
/////////////////////////////////////////////////////////////////////////

// REST PATTERN & Parameters
const [q1 , q2, ...q3] = [1,2,3,4,5,6,7,8,9,10];
console.log(q1, typeof q1);
console.log(q2, typeof q2);
console.log(q3, typeof q3);

const funcRestParameters = function(a,b,...c){
  console.log("I'm 'a' :" , a, typeof a);
  console.log("I'm 'b' :" , b, typeof b);
  console.log("I'm 'c' :" , c, typeof c);

}
funcRestParameters(1,2,343,45,"dasf",65,7,68,980,"a");

/// Short Circuiting
console.log(3 && "kobe");
console.log(true && 0);
console.log("" && "kobe");
console.log(undefined && null);

// coding challenge-1
/* Coding Challenge #1 
We're building a football betting app (soccer for my American friends 😅)! 
Suppose we get data from a web service about a certain game ('game' variable on 
next page). In this challenge we're gonna work with that data. 
Your tasks: 
1. Create one player array for each team (variables 'players1' and 
'players2') 
2. The first player in any player array is the goalkeeper and the others are field 
players. For Bayern Munich (team 1) create one variable ('gk') with the 
goalkeeper's name, and one array ('fieldPlayers') with all the remaining 10 
field players 
3. Create an array 'allPlayers' containing all players of both teams (22 
players) 
4. During the game, Bayern Munich (team 1) used 3 substitute players. So create a 
new array ('players1Final') containing all the original team1 players plus 
'Thiago', 'Coutinho' and 'Perisic' 
5. Based on the game.odds object, create one variable for each odd (called 
'team1', 'draw' and 'team2') 
6. Write a function ('printGoals') that receives an arbitrary number of player 
names (not an array) and prints each of them to the console, along with the 
number of goals that were scored in total (number of player names passed in) 
7. The team with the lower odd is more likely to win. Print to the console which 
team is more likely to win, without using an if/else statement or the ternary 
operator. 
Test data for 6.: First, use players 'Davies', 'Muller', 'Lewandowski' and 'Kimmich'. 
Then, call the function again with players from game.scored 
 
GOOD LUCK 😀*/

const game = { 
  team1: 'Bayern Munich', 
  team2: 'Borrussia Dortmund', 
  players: [ 
    [ 
      'Neuer', 
      'Pavard', 
      'Martinez', 
      'Alaba', 
      'Davies', 
      'Kimmich', 
      'Goretzka', 
      'Coman', 
      'Muller', 
      'Gnarby', 
      'Lewandowski', 
    ], 
    [ 
      'Burki', 
      'Schulz', 
      'Hummels', 
      'Akanji', 
      'Hakimi', 
      'Weigl', 
      'Witsel', 
      'Hazard', 
      'Brandt', 
      'Sancho', 
      'Gotze', 
    ], 
  ], 
  score: '4:0', 
  scored: ['Lewandowski', 'Gnarby', 'Lewandowski',  
  'Hummels'], 
  date: 'Nov 9th, 2037', 
  odds: { 
    team1: 1.33, 
    x: 3.25, 
    team2: 6.5, 
  }, 
}; 
// task-1
const  [players1,players2] = game.players;
console.log(players1);
console.log(players2); 

// task-2
const [gk,...fieldPlayers] = players1;
console.log(gk);
console.log(fieldPlayers); 

// task-3
const allPlayers = [...players1,...players2];
console.log(allPlayers);

// task-4
const players1Final = [...players1,'Thiago', 'Coutinho','Perisic'];
console.log(players1Final);

// task-5
const {odds:{team1,team2,x:draw}}= game;
console.log(team1,draw,team2);

// task-6
const printGoals = function(...playerNames){
  for (let player of playerNames){
    if (game["scored"].includes(player)){ 
    let individualGoals = 0;
    for (let i of game.scored){
      if (i===player) individualGoals +=1 ;
    }
    console.log(`${player} scored ${individualGoals} goals`);
    }
    else console.log(`${player} scored 0 goals`)
  }

}
printGoals('Lewandowski'
,'Neuer', 
'Pavard', 
'Martinez', 
'Alaba', 
'Davies', 
'Kimmich', 
'Goretzka', 
'Coman', 
'Muller', 
'Gnarby',
'Burki', 
'Schulz', 
'Hummels', 
'Akanji', 
'Hakimi', 
'Weigl', 
'Witsel', 
'Hazard', 
'Brandt', 
'Sancho', 
'Gotze',  );

// lec-112 optional chaining



// lec:115 - Sets
const alpha = new Set("jksdfnqoeijwnj b;qqqqqqqqqqwehjbvfowqiqwhejimc;hbsazlksaqopjiqw]");
console.log(alpha, typeof alpha,alpha.size); 

for (let i=0;i<26;i++){
  console.log(String.fromCharCode(97+i),alpha.has(String.fromCharCode(97+i)));
}
alpha.delete( 222);
// alpha.clear()
for (let i of alpha){  // ----> looping over a set
  console.log(i)
}

console.log(Array(...alpha));

for (let i of Object.entries(restaurant.openingHours)) console.log(i);

// lecture- 116 Maps
const rest = new Map();
rest.set([1,2,3],6); // array as a key
rest.set({"fname":"kobe" , "age":25},"NBA"); // object as a key 
rest.set("kylie","jenner"); // string as a key
rest.set(5,"paanch");  // Number as a key 
console.log(rest);

console.log(rest.set(1,"ek"));
rest.set(2,"do").set(3,"teen").set(4,"chaar");

console.log(rest.get(2) , typeof rest.get(2)); // get() method

console.log(rest.has("2")) // false because 2 === "2" is false


const var_arr = [1,2];   // use variables while using array/object/set as a key
rest.set(var_arr, "yes");
console.log(rest.has(var_arr));  // true
console.log(rest.get(var_arr));  // "yes"

rest.set(document.querySelector("h1"),"Data Structures and Modern Operators");
/// 👆 Above is the map with "document querySelector" as a key

console.log(Object.entries(person))

// for (let i in rest){
//   console.log(i);
// }
for (let i of rest){
  console.log(i);
}
console.log(Array(...rest));

// Working with strings
const str = "TAP Air Portugal";
console.log(str.slice(str.indexOf(' ')+1,str.lastIndexOf(' ')));
console.log("qwerty".slice(-2));

// fixing capitalization in a name
const passengerName = "hImaNsHU";
const correctName = passengerName[0].toUpperCase() + (passengerName.slice(1)).toLowerCase();
console.log(correctName);  // O/P:  "Himanshu"

// replace() method of strings
const str_org = "Mr Blue has a blue house and a blue car";
console.log(str_org.replaceAll('blue', 'red'));
console.log(str_org.replace(/blue|a/gi, 'red'));

const nname = 'Kobe Bryant';
const [fname1, lname1] = nname.split(" ");
const [...new_name] = [fname1 , lname1];
console.log(new_name);
console.log(new_name.join(" "));

console.log(nname.padStart((nname.length+"Mr. ".length) , "Mr. "));

console.log(nname.repeat(3.9));