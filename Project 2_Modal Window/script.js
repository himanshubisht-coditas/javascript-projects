"use strict";

// Storing selectors in const varibles
const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const btnCloseModal = document.querySelector(".close-modal");
const btnsOpenModal = document.querySelectorAll(".show-modal");

console.log(btnCloseModal);

// opening the modal window
for (let btn of btnsOpenModal){
    btn.addEventListener('click',function () {
        modal.classList.remove("hidden");
        overlay.classList.remove("hidden");
})
}

// close modal function
const closeModal = function(){
    modal.classList.add("hidden");
    overlay.classList.add("hidden");
}

// functionality of "X" in modal window
btnCloseModal.addEventListener("click",closeModal);

// when we click on the overlay, modal window should disappear
overlay.addEventListener("click", closeModal);

// adding esc key functionality on the whole page
document.addEventListener("keydown",function (key) {
    if (key["key"] == "Escape" && (!modal.classList.contains("hidden"))) closeModal();
});