'use strict';

///////////////////////////////////////
// Modal window

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');

const openModal = function (e) {
  e.preventDefault();
  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

const closeModal = function () {
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

btnsOpenModal.forEach(btn => btn.addEventListener("click" , openModal));

for (let i = 0; i < btnsOpenModal.length; i++)
  btnsOpenModal[i].addEventListener('click', openModal);

btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});


// selecting elements
console.log(document.querySelectorAll(".section"));
console.log(document.getElementById("section--1"));
console.log(document.getElementsByTagName("p"));

console.log(getComputedStyle(modal).color)
console.log(typeof getComputedStyle(modal).height)

document.documentElement.style.setProperty("--color-primary","blue")

console.log(modal.classList)

// smooth scrolling
const section1 = document.querySelector("#section--1")
console.log(section1.getBoundingClientRect())

// enent
section1.addEventListener('mouseenter' , function(e){
  console.log(Math.trunc(Math.random()*10000))
})

//dom  traversing
const h1 = document.querySelector("h1");
console.log(h1)

console.log(h1.childNodes);
console.log(h1.children);


const nav = document.querySelector(".nav")
const header = document.querySelector(".header");

const stickyNav = function(entries){
  const [entry] = entries;
  console.log(entry);
  if (entry.isIntersecting == false) nav.classList.add('sticky');
}

const headerObserver = new IntersectionObserver(stickyNav,{
  root : null,
  threshold  :0,
});
headerObserver.observe(header);


